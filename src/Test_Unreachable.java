// WELD_AT_OK
// WELD_BETWEEN_OK
public class Test_Unreachable {
	public static void m(int a) {
		Robot r = new Robot(0,100);
		if (a < 50 && a > 0) {
			if (a == 70) {
				r.weldAt(420);
				r.weldBetween(-10, 420);
			}
		}
	}
}
