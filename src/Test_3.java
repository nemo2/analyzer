// WELD_AT_NOT_OK
// WELD_BETWEEN_OK

//weld at is technically OK, but we do not handle * therefore we fail to stay sound
public class Test_3 {
	public static void m3(int a, int b) {
		Robot r = new Robot(-5, 5);
		if (a * b < 5 && a * b > -5 && b != 0) {
			r.weldAt(a + 1);
		}
	}
}
