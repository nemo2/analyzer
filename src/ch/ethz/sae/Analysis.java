package ch.ethz.sae;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import soot.IntegerType;
import soot.Local;
import soot.RefType;
import soot.SootClass;
import soot.SootField;
import soot.Unit;
import soot.Value;
import soot.jimple.AddExpr;
import soot.jimple.AssignStmt;
import soot.jimple.BinopExpr;
import soot.jimple.EnterMonitorStmt;
import soot.jimple.EqExpr;
import soot.jimple.ExitMonitorStmt;
import soot.jimple.GeExpr;
import soot.jimple.GotoStmt;
import soot.jimple.GtExpr;
import soot.jimple.IdentityStmt;
import soot.jimple.IfStmt;
import soot.jimple.IntConstant;
import soot.jimple.InvokeStmt;
import soot.jimple.LeExpr;
import soot.jimple.LookupSwitchStmt;
import soot.jimple.LtExpr;
import soot.jimple.MulExpr;
import soot.jimple.NeExpr;
import soot.jimple.NewExpr;
import soot.jimple.ParameterRef;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.Stmt;
import soot.jimple.SubExpr;
import soot.jimple.TableSwitchStmt;
import soot.jimple.internal.JNopStmt;
import soot.jimple.internal.JimpleLocal;
import soot.jimple.toolkits.annotation.logic.Loop;
import soot.toolkits.graph.LoopNestTree;
import soot.toolkits.graph.UnitGraph;
import soot.toolkits.scalar.ForwardBranchedFlowAnalysis;
import soot.util.Chain;
import sun.awt.resources.awt;
import apron.Abstract1;
import apron.ApronException;
import apron.Environment;
import apron.Manager;
import apron.MpqScalar;
import apron.Polka;
import apron.Tcons1;
import apron.Texpr1BinNode;
import apron.Texpr1CstNode;
import apron.Texpr1Intern;
import apron.Texpr1Node;
import apron.Texpr1VarNode;

// Implement your numerical analysis here.
public class Analysis extends ForwardBranchedFlowAnalysis<AWrapper> {

    private static final int WIDENING_THRESHOLD = 6;
    static final boolean IS_DEBUG_ENVIRONEMENT = false;

    private HashMap<Unit, Counter> loopHeads, backJumps;

    private void recordIntLocalVars() {

        Chain<Local> locals = g.getBody().getLocals();

        int count = 0;
        Iterator<Local> it = locals.iterator();
        while (it.hasNext()) {
            JimpleLocal next = (JimpleLocal) it.next();
            if (next.getType() instanceof IntegerType)
                count += 1;
        }

        local_ints = new String[count];

        int i = 0;
        it = locals.iterator();
        while (it.hasNext()) {
            JimpleLocal next = (JimpleLocal) it.next();
            String name = next.getName();
            if (next.getType() instanceof IntegerType)
                local_ints[i++] = name;
        }
    }

    private void recordIntClassVars() {

        Chain<SootField> ifields = jclass.getFields();

        int count = 0;
        Iterator<SootField> it = ifields.iterator();
        while (it.hasNext()) {
            SootField next = it.next();
            if (next.getType() instanceof IntegerType)
                count += 1;
        }

        class_ints = new String[count];

        int i = 0;
        it = ifields.iterator();
        while (it.hasNext()) {
            SootField next = it.next();
            String name = next.getName();
            if (next.getType() instanceof IntegerType)
                class_ints[i++] = name;
        }
    }

    /* Builds an environment with integer variables. */
    public void buildEnvironment() {

        recordIntLocalVars();
        recordIntClassVars();

        String ints[] = new String[local_ints.length + class_ints.length];

	/* add local ints */
        for (int i = 0; i < local_ints.length; i++) {
            ints[i] = local_ints[i];
        }

	/* add class ints */
        for (int i = 0; i < class_ints.length; i++) {
            ints[local_ints.length + i] = class_ints[i];
        }

        env = new Environment(ints, reals);
    }

    /* Instantiate a domain. */
    private void instantiateDomain() {
        man = new Polka(true);
    }

    /* === Constructor === */
    public Analysis(UnitGraph g, SootClass jc) {
        super(g);

        this.g = g;
        this.jclass = jc;

        buildEnvironment();
        instantiateDomain();

        loopHeads = new HashMap<Unit, Counter>();
        backJumps = new HashMap<Unit, Counter>();
        for (Loop l : new LoopNestTree(g.getBody())) {
            loopHeads.put(l.getHead(), new Counter(0));
            backJumps.put(l.getBackJumpStmt(), new Counter(0));
        }
    }

    void run() {
        doAnalysis();
    }

    /**
     * convert if stmt to Abstract1 objects
     * getDefault() contains the Abstract1 object for the then branch (condition passed)
     * getSecond() contains the Abstract1 object for the else branch (condition failed)
     */
    private PairResult<Abstract1> handleIfStmt(IfStmt expression,
                                               Abstract1 activeAbstract) throws ApronException {

        Value val = expression.getCondition();
        PairResult<Texpr1Intern> res = convertToApron(val);
        // default: left - right
        // second: right - left
        // example: if (a > b)
        // result is: getDefault: a - b
        // result is: getSecond: b- a
        if (res == null || 
        	res.getDefault() == null || 
        	(res.getSecond() == null && !(val instanceof EqExpr) && !(val instanceof NeExpr))
        	) {
            log("handleIfStmt", "convertToApron failed for " + val.getClass().getName());
            return null;
        }

        Abstract1 takeBranch;
        Abstract1 skipBranch;
        // meet == intersection
        // join == union
        if (val instanceof EqExpr) {
            // take branch if a - b == 0
            // take if a-b == 0
            takeBranch = activeAbstract.meetCopy(man, new Tcons1(Tcons1.EQ, res.getDefault()));            

            // skip if a - b <> 0
            skipBranch = activeAbstract.meetCopy(man, new Tcons1(Tcons1.DISEQ, res.getDefault()));
            // alternative solution:
        /*
         * skipBranch = activeAbstract.meetCopy(man, new Tcons1(
		 * Tcons1.SUP, res.getDefault())); Abstract1 skipBranch2 =
		 * activeAbstract.meetCopy(man, new Tcons1(Tcons1.SUP,
		 * res.getSecond())); skipBranch.join(man, skipBranch2);
		 */
        } else if (val instanceof NeExpr) {
            // take branch if a - b != 0
            // take if a-b != 0
            takeBranch = activeAbstract.meetCopy(man, new Tcons1(Tcons1.DISEQ, res.getDefault()));

            // skip if a - b == 0
            skipBranch = activeAbstract.meetCopy(man, new Tcons1(Tcons1.EQ, res.getDefault()));
        } else if (val instanceof GeExpr) {
            // take branch if a >= b
            // take if a-b >= 0
            takeBranch = activeAbstract.meetCopy(man, new Tcons1(Tcons1.SUPEQ, res.getDefault()));

            // skip if b-a > 0
            skipBranch = activeAbstract.meetCopy(man, new Tcons1(Tcons1.SUP, res.getSecond()));
        } else if (val instanceof GtExpr) {
            // take branch if a > b
            // take if a-b > 0
            takeBranch = activeAbstract.meetCopy(man, new Tcons1(Tcons1.SUP, res.getDefault()));

            // skip if b-a >= 0
            skipBranch = activeAbstract.meetCopy(man, new Tcons1(Tcons1.SUPEQ, res.getSecond()));
        } else if (val instanceof LeExpr) {
            // take branch if a <= b
            // take if b-a >= 0
            takeBranch = activeAbstract.meetCopy(man, new Tcons1(Tcons1.SUPEQ, res.getSecond()));

            // skip if a-b > 0
            skipBranch = activeAbstract.meetCopy(man, new Tcons1(Tcons1.SUP, res.getDefault()));
        } else if (val instanceof LtExpr) {
            // take branch if a < b
            // take if b-a > 0
            takeBranch = activeAbstract.meetCopy(man, new Tcons1(Tcons1.SUP, res.getSecond()));

            // skip if a-b >= 0
            skipBranch = activeAbstract.meetCopy(man, new Tcons1(Tcons1.SUPEQ, res.getDefault()));
        } else {
            log("handleIfStmt", "expression type not handled " + val.getClass().getName());
            return null;
        }
        return new PairResult<Abstract1>(takeBranch, skipBranch);
    }

    /**
     * convert value to apron
     */
    PairResult<Texpr1Intern> convertToApron(Value val) {
        if (val instanceof IntConstant) {
            return convertToApron((IntConstant) val);
        } else if (val instanceof JimpleLocal) {
            return convertToApron((JimpleLocal) val);
        } else if (val instanceof BinopExpr) {
            return convertToApron((BinopExpr) val);
        } else if (val instanceof ParameterRef || val instanceof NewExpr) {
            //Ignored
        } else {
            log("convertToApron(Value val)", "val type not handled " + val.getClass().getName());
        }
        return null;
    }

    /**
     * convert int constant
     */
    PairResult<Texpr1Intern> convertToApron(IntConstant constant) {
        // MpqScalar: wrapper for scalar values
        // pass it to the constant node: Texpr1CstNode
        // create a tree we can use with Texpr1Intern
        Texpr1Node rAr = new Texpr1CstNode(new MpqScalar(constant.value));
        return new PairResult<Texpr1Intern>(new Texpr1Intern(env, rAr));
    }

    /**
     * convert local variable to apron
     */
    PairResult<Texpr1Intern> convertToApron(JimpleLocal node) {
	//ignore RefType (class Robot for example)
	if (node.getType() instanceof RefType)
	    return null;
        Texpr1VarNode rAr = new Texpr1VarNode(node.getName());
        return new PairResult<Texpr1Intern>(new Texpr1Intern(env, rAr));
    }

    /**
     * convert Binary expression to apron
     */
    PairResult<Texpr1Intern> convertToApron(BinopExpr expression) {
        PairResult<Texpr1Intern> left = convertToApron(expression.getOp1());
        PairResult<Texpr1Intern> right = convertToApron(expression.getOp2());

        if (left == null || left.getDefault() == null || right == null || right.getDefault() == null) {
            log("convertToApron(BinopExpr expression)", "failed to convert to apron either " + expression.getOp1().getClass().getName() + " or " + expression.getOp2().getClass().getName());
            return null;
        }

        // handle add, subb & mul
        if (expression instanceof AddExpr || expression instanceof SubExpr
                || expression instanceof MulExpr) {
            int op = Texpr1BinNode.OP_ADD;
            if (expression instanceof SubExpr) {
                op = Texpr1BinNode.OP_SUB;
            } else if (expression instanceof MulExpr) {
                op = Texpr1BinNode.OP_MUL;
            }

            Texpr1BinNode mul = new Texpr1BinNode(
                    op,
                    left.getDefault().toTexpr1Node(),
                    right.getDefault().toTexpr1Node()
            );
            return new PairResult<Texpr1Intern>(new Texpr1Intern(env, mul));
        } else if (expression instanceof EqExpr || expression instanceof NeExpr) {
            // if equal or not equal we need only a-b
            Texpr1BinNode leftMinusRight = new Texpr1BinNode(
                    Texpr1BinNode.OP_SUB,
                    left.getDefault().toTexpr1Node(),
                    right.getDefault().toTexpr1Node()
            );
            return new PairResult<Texpr1Intern>(new Texpr1Intern(env, leftMinusRight));
        } else if (expression instanceof GtExpr || expression instanceof LeExpr
                || expression instanceof LtExpr || expression instanceof GeExpr) {
            // need a-b && b- a
            Texpr1BinNode leftMinusRight = new Texpr1BinNode(
                    Texpr1BinNode.OP_SUB,
                    left.getDefault().toTexpr1Node(),
                    right.getDefault().toTexpr1Node()
            );
            Texpr1BinNode rightMinusLeft = new Texpr1BinNode(
                    Texpr1BinNode.OP_SUB,
                    right.getDefault().toTexpr1Node(),
                    left.getDefault().toTexpr1Node()
            );
            return new PairResult<Texpr1Intern>(
                    new Texpr1Intern(env, leftMinusRight),
                    new Texpr1Intern(env, rightMinusLeft)
            );
        } else {
            log("convertToApron(BinopExpr expression)", "expression type not handled " + expression.getClass().getName());
            return null;
        }

    }

    /**
     * convert identity to Abstract1
     */
    private Abstract1 handleIdentityStmt(IdentityStmt identity, Abstract1 in)
            throws ApronException {
        Value left = identity.getLeftOp();
        Value right = identity.getRightOp();

        Abstract1 out = in;
        if (left instanceof JimpleLocal) {
            // get the name of the target value
            String varName = ((JimpleLocal) left).getName();

            //convert to apron representation
            PairResult<Texpr1Intern> rightNode = convertToApron(right);
            if (rightNode != null && rightNode.getDefault() != null) {
                out = in.assignCopy(man, varName, rightNode.getDefault(), null);
            }
        }

        return new Abstract1(man, out);
    }

    /**
     * convert assignment to Abstract1
     */
    private Abstract1 handleAssignStmt(AssignStmt identity, Abstract1 in)
            throws ApronException {
        Value valLeft = identity.getLeftOp();
        Value valRight = identity.getRightOp();

        Abstract1 out = in;
        if (valLeft instanceof JimpleLocal) {
            // get the name of the target value
            JimpleLocal castedVal = (JimpleLocal) valLeft;
            String name = castedVal.getName();

            //convert to apron representation
            PairResult<Texpr1Intern> apronRight = convertToApron(valRight);
            if (apronRight != null && apronRight.getDefault() != null) {
                out = in.assignCopy(man, name, apronRight.getDefault(), null);
            }
        }

        return new Abstract1(man, out);
    }

    @Override
    protected void flowThrough(AWrapper inWrapper, Unit op,
                               List<AWrapper> fallOutWrappers, List<AWrapper> branchOutWrappers) {
    	log("flowThrough", op.toString() + " "+ inWrapper.toString());
        Stmt activeStatement = (Stmt) op;
        Abstract1 activeAbstract = inWrapper.get();

        try {
            Abstract1 fallInAbstract = null;
            Abstract1 fallOutAbstract = null;

            // check for different possible types, and call appropriate methods
            // DefinitionStmt is an interface, AbstractDefintionStmt provides
            // base functionality, concrete implementation has the J prepended,
            // so JDefintion:
            // IdentityStmt, AssignStmt
            if (activeStatement instanceof IdentityStmt) {
                // statement is of the form: a = b;
                IdentityStmt castedStatement = (IdentityStmt) activeStatement;
                fallInAbstract = handleIdentityStmt(castedStatement, activeAbstract);
            } else if (activeStatement instanceof AssignStmt) {
                // statement is of the form: a = b; ??
                //TODO: What is difference between IdentityStmt && AssignStmt?
                AssignStmt castedStatement = (AssignStmt) activeStatement;
                fallInAbstract = handleAssignStmt(castedStatement, activeAbstract);
            } else if (activeStatement instanceof IfStmt) {
                // statement of the form: if (a)
                IfStmt castedStatement = (IfStmt) activeStatement;
                PairResult<Abstract1> res = handleIfStmt(castedStatement, activeAbstract);
                if (res != null) {
                    fallOutAbstract = res.getDefault();
                    fallInAbstract = res.getSecond();
                }
            } else if (activeStatement instanceof GotoStmt) {
            	GotoStmt castedStmt = (GotoStmt)activeStatement;
            	Unit target = castedStmt.getTarget();
            	// I think we can just ignore this, because soot should handle this
        	
            	log("flowThrough: not implemented", "gotoStmt");
            } else if (activeStatement instanceof TableSwitchStmt) {
            	log("flowTrhough: not implemented", "TableSwitchStmtStmt");
            } else if (activeStatement instanceof LookupSwitchStmt) {
            	log("flowTrhough: not implemented", "gotoLookupSwitchStmt");
            } else if (activeStatement instanceof JNopStmt || // no operation
                    activeStatement instanceof EnterMonitorStmt || // monitor stuff
                    activeStatement instanceof ExitMonitorStmt ||
                    activeStatement instanceof ReturnVoidStmt ||  // return statements
                    activeStatement instanceof ReturnStmt ||
                    activeStatement instanceof InvokeStmt // invoke method
                    ) {
                //ignored because no function calls can occur in the analyzed code
            } else {
                log("flowThrough", "this statment type is not supported yet: " + activeStatement.getClass().getName());
            }

            // if the abstract is null, take the input abstract and simply pass it on
            if (fallInAbstract == null) {
            	fallInAbstract = activeAbstract;
            }
            AWrapper aw = new AWrapper(fallInAbstract, man);
            for (AWrapper w : fallOutWrappers)
                copy(aw, w);

            if (fallOutAbstract == null) {
            	fallOutAbstract = activeAbstract;
            }
            aw = new AWrapper(fallOutAbstract, man);
            for (AWrapper w : branchOutWrappers)
                copy(aw, w);
            
            aw.get();
        } catch (ApronException e) {
            log("flowThrough", "exception occurred: " + e.toString());
        }
    }

    static void log(String source, String stuff) {
        if (IS_DEBUG_ENVIRONEMENT) {
            System.out.println("[ERROR in " + source + "]: " + stuff);
        }
    }

    static void logInfo(String source, String stuff) {
        if (IS_DEBUG_ENVIRONEMENT) {
            System.out.println("[INFO in " + source + "]: " + stuff);
        }
    }

    @Override
    protected void copy(AWrapper source, AWrapper dest) {
        try {
            dest.set(new Abstract1(man, source.get()));
        } catch (ApronException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected AWrapper entryInitialFlow() {
        Abstract1 top = null;
        try {
            top = new Abstract1(man, env);
        } catch (ApronException e) {
        }
        return new AWrapper(top, man);
    }

    private static class Counter {
        int value;

        Counter(int v) {
            value = v;
        }
    }

    @Override
    protected void merge(Unit succNode, AWrapper w1, AWrapper w2, AWrapper w3) {
        Counter count = loopHeads.get(succNode);

        Abstract1 a1 = w1.get();
        Abstract1 a2 = w2.get();
        Abstract1 a3 = null;

        try {
            if (count != null) {
                ++count.value;
                if (count.value < WIDENING_THRESHOLD) {
                    a3 = a1.joinCopy(man, a2);
                } else {
                    a3 = a1.widening(man, a2);
                }
            } else {
                a3 = a1.joinCopy(man, a2);
            }
            w3.set(a3);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Override
    protected void merge(AWrapper src1, AWrapper src2, AWrapper trg) {

        Abstract1 a1 = src1.get();
        Abstract1 a2 = src2.get();
        Abstract1 a3 = null;

        try {
            a3 = a1.joinCopy(man, a2);
        } catch (ApronException e) {
            e.printStackTrace();
        }
        trg.set(a3);
    }

    @Override
    protected AWrapper newInitialFlow() {
        Abstract1 bot = null;

        try {
            bot = new Abstract1(man, env, true);
        } catch (ApronException e) {
        }
        AWrapper a = new AWrapper(bot, man);
        a.man = man;
        return a;

    }

    public static final boolean isIntValue(Value val) {
        return val.getType().toString().equals("int")
                || val.getType().toString().equals("short")
                || val.getType().toString().equals("byte");
    }

    public static Manager man;
    public static Environment env;
    public UnitGraph g;
    public String local_ints[]; // integer local variables of the method
    public static String reals[] = {"x"};
    public SootClass jclass;
    private String class_ints[]; // integer class variables where the method is
    // defined
}

final class PairResult<T> {
    private T def;
    private T second;

    public PairResult(T def) {
        this.def = def;
    }

    public PairResult(T def, T second) {
        this.def = def;
        this.second = second;
    }

    public T getDefault() {
        return this.def;
    }

    public T getSecond() {
        return this.second;
    }
}