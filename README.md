# Analyzer

Built with soot & apron

project: http://www.srl.ethz.ch/sae17project2.php

## Architecture
- In Analyzer.java the soot output is converted to apron objects
- In Verifier.java the properties which need to hold for method calls are evaluated using the apron fixpoint & reference analysis

## Soot
responsible for converting java bytecode of a to-be-tested class into a representation usable for analyzation. Resources:
 - survivel guide: http://www.brics.dk/SootGuide/sootsurvivorsguide.pdf
 - github: https://github.com/Sable/soot (build failing)
 - "docs": https://www.sable.mcgill.ca/soot/doc/soot/jimple/

### Architecture of Soot
 - pretty well made, a lot of inheritance and interfaces. The naming is as follows: 
   - `AbstractDefinitionStmt`: abstract base classes which are extended by concrete implementations
   - `DefinitionStmt`: The interface of the implementation (always reference this one in code!)
   - `JDefinitionStmt`: The concrete implementation
 
### guidelines
 - always use the interface in code
 - in `Analyzer.java` add for each `Stmt` type an `instanceof` clause, and then call a method which deals with this

## APRON
responsible for dealing with the polyhedra domain. Does all the fancy stuff! We are using a java wrapper provided by ETH.
Resources:
 - webpage: http://apron.cri.ensmp.fr
 - java wrapper: http://www.srl.inf.ethz.ch/sae2016/doc/index.html

## Building & Testing

To test all cases run `make test`, for running a single test file run e.g. `make test_1`.
There are generated testcases as well as handmades. 

## Make it better
 - polyhedra domain is bad for inequalities; deal with that (it is possible!)!
 - more testcases may reveal more crashes.