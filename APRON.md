# APRON

## API overview
`MpqScalar`: Wrapper for a rational number. If only given one parameter; selves as wrapper for integer  
`Texpr1CstNode`: Node in the expression tree which takes a `Coefficient` as argument
`Coefficient`

`Texpr1Node`: A level 0 node
- `Texpr1BinNode`: binary node with ADD, DIV, MUL (takes operator (constant in class, similar to `OP_ADD`), left argument (`Texpr0Node`), right argument (`Texpr0Node`)  
- `Texpr1CstNode`: Constant leave (takes `Coeff` as constructor argument)
- `Texpr1UnNode`: Unary node (takes operator (constant in class, similar to `OP_CAST`), argument (`Texpr0Node`))  
etc    

## general insights

APRON is a java wrapper for a c library, and does the transformation of the intervals. It has some bugs (shows bottom where it should not show bottom), see the mailing list for workarounds.

### Difference between level0 and level1

tl&dr: just use level 1  
level 0 is optimized for speed, so all variable names will be replaced with some fast ones (don't ask me why), if we use level 1 we'll still be able to see the actual variables as used in the code

