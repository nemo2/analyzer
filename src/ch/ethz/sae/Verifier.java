package ch.ethz.sae;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import apron.Abstract1;
import apron.DoubleScalar;
import apron.Lincons1;
import soot.jimple.IntConstant;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.internal.JimpleLocal;
import soot.jimple.internal.JimpleLocalBox;
import soot.jimple.spark.SparkTransformer;
import soot.jimple.spark.pag.Node;
import soot.jimple.spark.pag.PAG;
import soot.jimple.spark.pag.VarNode;
import soot.jimple.spark.sets.P2SetVisitor;
import soot.jimple.spark.sets.PointsToSetInternal;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Unit;
import soot.Value;
import soot.toolkits.graph.BriefUnitGraph;

public class Verifier {
	
	private static void log(String source, String stuff, boolean isError) {
	    if (isError)
		Analysis.log(source, stuff);
	    else
		Analysis.logInfo(source, stuff);
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			System.err
					.println("Usage: java -classpath soot-2.5.0.jar:./bin ch.ethz.sae.Verifier <class to test>");
			System.exit(-1);
		}
		String analyzedClass = args[0];
		SootClass c = loadClass(analyzedClass);

		int weldAtFlag = 1;
		int weldBetweenFlag = 1;
		
		try {
			PAG pointsToAnalysis = doPointsToAnalysis(c);


			for (SootMethod method : c.getMethods()) {
	
				if (method.getName().contains("<init>")) {
					// skip constructor of the class
					continue;
				}
				Analysis analysis = new Analysis(new BriefUnitGraph(
						method.retrieveActiveBody()), c);
				analysis.run();
	
				Map<Integer, Value[]> robots = findRobotConfigurations(method,
						analysis, pointsToAnalysis);
				
				try {
					if (!verifyWeldAt(method, analysis, pointsToAnalysis, robots)) {
						weldAtFlag = 0;
					}
				} catch (Throwable e) {
					// we can't verify this
					weldAtFlag = 0;
					if(Analysis.IS_DEBUG_ENVIRONEMENT) {
						e.printStackTrace();
					}
				}
				try {
					if (!verifyWeldBetween(method, analysis, pointsToAnalysis,
							robots)) {
						weldBetweenFlag = 0;
					}
				} catch (Throwable e) {
					// we can't verify this
					weldBetweenFlag = 0;
					if(Analysis.IS_DEBUG_ENVIRONEMENT) {
						e.printStackTrace();
					}
				}
			}
		} catch(Throwable e) {
			if(Analysis.IS_DEBUG_ENVIRONEMENT) {
				e.printStackTrace();
			}
			weldAtFlag = 0;
			weldBetweenFlag = 0;
		}

		// Do not change the output format
		if (weldAtFlag == 1) {
			System.out.println(analyzedClass + " WELD_AT_OK");
		} else {
			System.out.println(analyzedClass + " WELD_AT_NOT_OK");
		}
		if (weldBetweenFlag == 1) {
			System.out.println(analyzedClass + " WELD_BETWEEN_OK");
		} else {
			System.out.println(analyzedClass + " WELD_BETWEEN_NOT_OK");
		}
	}

	private static Map<Integer, Value[]> findRobotConfigurations(
			SootMethod method, Analysis fixPoint, PAG pag) {
		// There is no Tuple in Java 6, so we use an Array here
		final Map<Integer, Value[]> robots = new HashMap<Integer, Value[]>();
		
		for (Unit unit : method.getActiveBody().getUnits()) {
			if (unit instanceof InvokeStmt) {
				InvokeStmt stmt = (InvokeStmt) unit;
				InvokeExpr expr = stmt.getInvokeExpr();
				if (expr.getMethod().getName().equals("<init>")) {
					// handle the constructor
					final Value[] values = new Value[2];
					values[0] = expr.getArg(0);
					values[1] = expr.getArg(1);
					
					// find the robots Node in the graph
					JimpleLocalBox robot = (JimpleLocalBox) stmt.getUseBoxes()
							.get(0);
					VarNode node = pag.findLocalVarNode(robot.getValue());
					if (node == null) {
						node = pag.findGlobalVarNode(robot.getValue());
					}
					if (node == null) {
						// TODO: we cannot handle this robot
						continue;
					}
					
					// assign the interval to the target node
					// (should only be one)
					node.getP2Set().forall(new P2SetVisitor() {
						@Override
						public void visit(Node node) {
							robots.put(node.getNumber(), values);
						}
					});
				}
			}
		}
		return robots;
	}

	/**
	 * Verifies whether the constraint (greater >= less) or (greater > less)
	 * holds always true (strict = true or false respectively).
	 * 
	 * Returns true only if the constraint provably holds.
	 * 
	 * Some constraint equalities used in the following:
	 *     greater >= less
	 * <=> greater - less >= 0
	 * <=> ¬(greater - less < 0)
	 * <=> ¬(less - greater > 0)
	 * <=> ¬(less - greater - 1 >= 0)
	 * 
	 * 	   greater > less
	 * <=> greater - less > 0
	 * <=> ¬(greater - less <= 0)
	 * <=> ¬(less - greater >= 0)
	 */
	private static boolean verifyLess(Abstract1 flow,
			Value less,  Value greater, boolean strict) throws Exception {
		Lincons1 constraint = new Lincons1(Analysis.env);
		int constant = 0;

		if (!strict) {
			constant--;
		}

		if (greater instanceof IntConstant) {
			constant -= ((IntConstant) greater).value;
		} else if (greater instanceof JimpleLocal) {
			constraint.setCoeff(((JimpleLocal) greater).getName(),
					new DoubleScalar(-1));
		} else {
			throw new Exception(
					"Can only handle Constants and Locals in verifyLess (greater)");
		}

		if (less instanceof IntConstant) {
			constant += ((IntConstant) less).value;
		} else if (less instanceof JimpleLocal) {
			constraint.setCoeff(((JimpleLocal) less).getName(),
					new DoubleScalar(1));
		} else {
			throw new Exception(
					"Can only handle Constants and Locals in verifyLess (less)");
		}

		constraint.setCst(new DoubleScalar(constant));
		log("verifyLess", "checking constraint ¬(" + constraint.toString() + ")", false);
		log("verifyLess", "i.e. "+ less.toString() +" < "+ greater.toString(), false);
		
		Abstract1 constrainedFlow = flow.meetCopy(Analysis.man, constraint);
		return constrainedFlow.isBottom(Analysis.man);
	}
	
	private static Set<Integer> findAllocationNodeNumbers(InvokeStmt stmt, PAG pointsTo) {
		JimpleLocalBox robot = (JimpleLocalBox) stmt.getUseBoxes().get(0);
		VarNode node = pointsTo.findLocalVarNode(robot.getValue());
		PointsToSetInternal possibleRobots = node.getP2Set();
		final Set<Integer> possibleNodeNumbers = new LinkedHashSet<Integer>();
		possibleRobots.forall(new P2SetVisitor() {
			@Override
			public void visit(Node node) {
				possibleNodeNumbers.add(node.getNumber());
			}
		});
		
		log("findAllocationNodeNumbers", "found " + possibleNodeNumbers.toString(), false);
		return possibleNodeNumbers;
	}

	/**
	 * Checks Property 2:
     * For any reachable invocation of weldBetween(startPoint, endPoint) on
     * an object r of class Robot, the following holds:
     * 	r.left <= startPoint < endPoint <= r.right
	 */
	private static boolean verifyWeldBetween(SootMethod method,
			Analysis fixPoint, PAG pointsTo, Map<Integer, Value[]> robots)
			throws Exception {
		if (method.isAbstract()) {
			return true;
		}

		for (Unit unit : method.getActiveBody().getUnits()) {
			if (unit instanceof InvokeStmt) {
				InvokeStmt stmt = (InvokeStmt) unit;
				InvokeExpr expr = stmt.getInvokeExpr();
				if (expr.getMethod().getName().equals("weldBetween")) {
					Abstract1 flow = fixPoint.getFlowBefore(unit).get();
					if (flow.isBottom(Analysis.man)) {
						// bottom means unreachable code -> ignore the call
						log("verifyWeldBetween", "unreachable Code", false);
						continue;
					}
					
					final Value start = expr.getArg(0);
					final Value end = expr.getArg(1);

					// find possible allocation nodes
					Set<Integer> possibleNodeNumbers = findAllocationNodeNumbers(stmt, pointsTo);
					
					// verify the constraints for each possible robot
					for (int number : possibleNodeNumbers) {
						Value[] arguments = robots.get(number);
						Value left = arguments[0];
						Value right = arguments[1];
						
						// left <= startPoint
						if (!verifyLess(flow, left, start, false)) {
							return false;
						}
						
						// startPoint < endPoint
						if (!verifyLess(flow, start, end, true)) {
							return false;
						}
						
						// endPoint <= right
						if (!verifyLess(flow, end, right, false)) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	/**
	 * Property 1:
     * For any reachable invocation of weldAt(point) on an object
     * r of class Robot, the following holds:
     *  r.left <= point <= r.right
	 */
	private static boolean verifyWeldAt(SootMethod method, Analysis fixPoint,
			PAG pointsTo, Map<Integer, Value[]> robots) throws Exception {
		if (method.isAbstract()) {
			return true;
		}
		
		for (Unit unit : method.getActiveBody().getUnits()) {
			if (unit instanceof InvokeStmt) {
				InvokeStmt stmt = (InvokeStmt) unit;
				InvokeExpr expr = stmt.getInvokeExpr();
				if (expr.getMethod().getName().equals("weldAt")) {
					Abstract1 flow = fixPoint.getFlowBefore(unit).get();
					if (flow.isBottom(Analysis.man)) {
						// bottom means unreachable code -> ignore the call
						log("verifyWeldAt", "unreachable Code", false);
						continue;
					}
					
					final Value point = expr.getArg(0);

					// find possible allocation nodes
					Set<Integer> possibleNodeNumbers = findAllocationNodeNumbers(stmt, pointsTo);
					
					// verify the constraints for each possible robot
					for (int number : possibleNodeNumbers) {
						Value[] arguments = robots.get(number);
						Value left = arguments[0];
						Value right = arguments[1];
						
						// left <= point
						log("verifyWeldAt", left.toString() + " <= "+ point.toString(), false);
						log("verifyWeldAt", flow.toString(), false);
						if (!verifyLess(flow, left, point, false)) {
							return false;
						}
						
						// point <= right
						if (!verifyLess(flow, point, right, false)) {
							return false;
						}
						
						// left < right
						if (!verifyLess(flow, left, right, true)) {
							return false;
						}
					}
				}
			}
		}
		return true;
		
	}

	private static SootClass loadClass(String name) {
		SootClass c = Scene.v().loadClassAndSupport(name);
		c.setApplicationClass();
		return c;
	}

	// Performs Points-To Analysis
	private static PAG doPointsToAnalysis(SootClass c) {
		Scene.v().setEntryPoints(c.getMethods());

		HashMap<String, String> options = new HashMap<String, String>();
		options.put("enabled", "true");
		options.put("verbose", "false");
		options.put("propagator", "worklist");
		options.put("simple-edges-bidirectional", "false");
		options.put("on-fly-cg", "true");
		options.put("set-impl", "double");
		options.put("double-set-old", "hybrid");
		options.put("double-set-new", "hybrid");

		SparkTransformer.v().transform("", options);
		PAG pag = (PAG) Scene.v().getPointsToAnalysis();

		return pag;
	}

}
